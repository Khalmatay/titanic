import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+)\.', expand=False)

    result = []

    for title in ["Mr", "Mrs", "Miss"]:
        median_age = df.loc[df['Title'] == title, 'Age'].median().round().astype(int)

        missing_ages_count = df.loc[df['Title'] == title, 'Age'].isnull().sum()

        df.loc[(df['Age'].isnull()) & (df['Title'] == title), 'Age'] = median_age

        result.append((title + '.', missing_ages_count, median_age))


    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    return result
